# MangroveSupportLibraryiOS

[![CI Status](http://img.shields.io/travis/Tom Spee/MangroveSupportLibraryiOS.svg?style=flat)](https://travis-ci.org/Tom Spee/MangroveSupportLibraryiOS)
[![Version](https://img.shields.io/cocoapods/v/MangroveSupportLibraryiOS.svg?style=flat)](http://cocoapods.org/pods/MangroveSupportLibraryiOS)
[![License](https://img.shields.io/cocoapods/l/MangroveSupportLibraryiOS.svg?style=flat)](http://cocoapods.org/pods/MangroveSupportLibraryiOS)
[![Platform](https://img.shields.io/cocoapods/p/MangroveSupportLibraryiOS.svg?style=flat)](http://cocoapods.org/pods/MangroveSupportLibraryiOS)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MangroveSupportLibraryiOS is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MangroveSupportLibraryiOS'
```

## Author

Tom Spee, tom@mobilepioneers.com

## License

MangroveSupportLibraryiOS is available under the MIT license. See the LICENSE file for more info.
