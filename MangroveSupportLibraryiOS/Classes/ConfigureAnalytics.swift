public class MangroveSupportAnalytics {
    
    public static let instance: MangroveSupportAnalytics = MangroveSupportAnalytics()
    
    public init() {}
    
    public enum AnalyticsType {
        case Salesforce
    }
    
    public var kruxTracker: KruxTracker!
    public func getKruxTracker() -> KruxTracker {
        return kruxTracker
    }

    public func configure(type: AnalyticsType, appID: String){
        if(type == .Salesforce){
            kruxTracker = KruxTracker.sharedEventTracker(withConfigId: appID, debugFlag: false, dryRunFlag: false)
        }
    }
    
    public func trackScreen(screenName: String, customData: NSDictionary){
        kruxTracker.trackPageView(screenName, attributes: customData as! [AnyHashable : Any])
    }
    
}
