#
# Be sure to run `pod lib lint MangroveSupportLibraryiOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MangroveSupportLibrary'
  s.version          = '1.0.0'
  s.summary          = 'This is a support library for iOS from Mangrove.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This support library for iOS is created by Mangrove to use in different projects'

  s.homepage         = 'https://bitbucket.org/mangrove/mangrove-pods'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tom Spee' => 'tom@mobilepioneers.com' }
  s.source           = { :git => 'https://bitbucket.org/mangrove/mangrove-pods', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'MangroveSupportLibraryiOS/Classes/**/*', 'MangroveSupportLibraryiOS/Frameworks/*'
  
  # s.resource_bundles = {
  #   'MangroveSupportLibraryiOS' => ['MangroveSupportLibraryiOS/Assets/*.png']
  # }

  s.vendored_frameworks = 'iOSKruxLibUniversal.framework'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit', 'SystemConfiguration', 'AdSupport'
  # s.dependency 'AFNetworking', '~> 2.3'
end
