//
//  ViewController.swift
//  MangroveSupportLibraryiOS
//
//  Created by Tom Spee on 11/22/2017.
//  Copyright (c) 2017 Tom Spee. All rights reserved.
//

import UIKit
import MangroveSupportLibraryiOS

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        MangroveSupportAnalytics.instance.trackScreen(screenName: "Home screen", customData: ["action": "open_screen", "screen": "home"])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

